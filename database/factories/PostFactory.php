<?php

/** @var Factory $factory */

use App\Models\Post\Post;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Post::class, function (Faker $faker) {
    return [
        Post::COLUMN_TITLE => $faker->sentence,
        Post::COLUMN_TEXT => $faker->paragraph,
        Post::CREATED_AT => Carbon::now(),
        Post::UPDATED_AT => Carbon::now(),
    ];
});
