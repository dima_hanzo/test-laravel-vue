<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Comment\Comment;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

$factory->define(Comment::class, function (Faker $faker) {
    return [
       Comment::COLUMN_POST_ID => null,
       Comment::COLUMN_PARENT_ID => null,
       Comment::COLUMN_TEXT => $faker->paragraph,
       Comment::CREATED_AT => Carbon::now(),
       Comment::UPDATED_AT => Carbon::now(),
    ];
});
