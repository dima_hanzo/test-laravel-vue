<?php

use App\Models\Comment\Comment;
use App\Models\Post\Post;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @var Post $post
         */
        factory(Post::class, 1)->create()->each(function ($post) {
            $post->comments()->saveMany(
                factory(Comment::class, 3)
                    ->create([Comment::COLUMN_POST_ID => $post->id])
                    ->each(function ($comment) use ($post) {
                        $comment->child()->saveMany(
                            factory(Comment::class, 3)->create([
                                Comment::COLUMN_POST_ID => $post->id,
                            ]));
                    }));
        });
    }
}
