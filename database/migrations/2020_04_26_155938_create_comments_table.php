<?php

use App\Models\Comment\Comment;
use App\Models\Post\Post;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Comment::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements(Comment::COLUMN_ID);
            $table->unsignedBigInteger(Comment::COLUMN_PARENT_ID)->nullable();
            $table->unsignedBigInteger(Comment::COLUMN_POST_ID);
            $table->text(Comment::COLUMN_TEXT);
            $table->timestamps();

            $table->foreign(Comment::COLUMN_PARENT_ID)
                ->references(Comment::COLUMN_ID)
                ->on(Comment::TABLE_NAME)
                ->onDelete('cascade');

            $table->foreign(Comment::COLUMN_POST_ID)
                ->references(Post::COLUMN_ID)
                ->on(Post::TABLE_NAME)
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Comment::TABLE_NAME);
    }
}
