<?php

use App\Models\Post\Post;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Post::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements(Post::COLUMN_ID);
            $table->string(Post::COLUMN_TITLE);
            $table->text(Post::COLUMN_TEXT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Post::TABLE_NAME);
    }
}
