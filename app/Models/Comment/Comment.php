<?php

namespace App\Models\Comment;

use App\Models\Post\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Comment extends Model
{
    const TABLE_NAME = 'comments';

    const COLUMN_ID = 'id';
    const COLUMN_PARENT_ID = 'parent_id';
    const COLUMN_POST_ID = 'post_id';
    const COLUMN_TEXT = 'text';

    const FIELD_CHILD = 'child';

    /**
     * @var string $table
     */
    protected $table = self::TABLE_NAME;

    /**
     * @var array $fillable
     */
    protected $fillable = [
        self::COLUMN_POST_ID,
        self::COLUMN_PARENT_ID,
        self::COLUMN_TEXT,
        self::CREATED_AT,
        self::UPDATED_AT,
    ];

    /**
     * @return BelongsTo
     */
    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * @return HasMany
     */
    public function child(): HasMany
    {
        return $this->hasMany(static::class, static::COLUMN_PARENT_ID, static::COLUMN_ID)
            ->orderBy(self::UPDATED_AT,'desc')
            ->with('child');
    }

    /**
     * @return BelongsToMany
     */
    public function parent(): BelongsToMany
    {
        return $this->belongsToMany(static::class, static::TABLE_NAME, static::COLUMN_ID, static::COLUMN_PARENT_ID)
            ->orderBy(self::UPDATED_AT,'desc')
            ->with('child');
    }
}
