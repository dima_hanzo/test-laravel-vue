<?php

namespace App\Models\Post;

use App\Models\Comment\Comment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    const TABLE_NAME = 'posts';

    const COLUMN_ID = 'id';
    const COLUMN_TITLE = 'title';
    const COLUMN_TEXT = 'text';

    const FIELD_COMMENTS = 'comments';

    /**
     * @var string $table
     */
    protected $table = self::TABLE_NAME;

    /**
     * @var array $fillable
     */
    protected $fillable = [
        self::COLUMN_TITLE,
        self::COLUMN_TEXT,
        self::CREATED_AT,
        self::UPDATED_AT,
    ];

    /**
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class)
            ->whereNull(Comment::COLUMN_PARENT_ID)
            ->orderBy(self::UPDATED_AT,'desc')
            ->with('child');
    }
}
