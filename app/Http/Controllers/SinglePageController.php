<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

class SinglePageController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('layouts/app');
    }
}
