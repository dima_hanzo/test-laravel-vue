<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\CommentRequest;
use App\Http\Resources\Comment\CommentResource;
use App\Models\Comment\Comment;
use App\Repositories\Comment\CommentRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param CommentRequest $commentRequest
     * @return CommentResource
     */
    public function store(CommentRequest $commentRequest): CommentResource
    {
        return CommentResource::make((new CommentRepository())->create($commentRequest->validated()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CommentRequest $commentRequest
     * @param Comment $comment
     * @return CommentResource
     */
    public function update(CommentRequest $commentRequest, Comment $comment): CommentResource
    {
        return CommentResource::make((new CommentRepository())->update($commentRequest->validated(), $comment));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id): JsonResponse
    {   (new CommentRepository())->delete($id);
        return response()->json(null, 204);
    }
}
