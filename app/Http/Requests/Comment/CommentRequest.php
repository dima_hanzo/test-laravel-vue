<?php

namespace App\Http\Requests\Comment;

use App\Models\Comment\Comment;
use App\Models\Post\Post;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Comment::COLUMN_PARENT_ID => [
                'nullable',
                Rule::exists(Comment::TABLE_NAME, Comment::COLUMN_ID)
            ],
            Comment::COLUMN_POST_ID => [
                'required',
                Rule::exists(Post::TABLE_NAME, Post::COLUMN_ID)
            ],
            Comment::COLUMN_TEXT => [
                'required',
                'string'
            ],
            Comment::CREATED_AT => [
                'nullable',
                'string'
            ],
            Comment::UPDATED_AT => [
                'nullable',
                'string'
            ],
        ];
    }
}
