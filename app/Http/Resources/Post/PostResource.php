<?php

namespace App\Http\Resources\Post;

use App\Models\Post\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            Post::COLUMN_ID => $this->id,
            Post::COLUMN_TITLE => $this->title,
            Post::COLUMN_TEXT => $this->text,
            Post::FIELD_COMMENTS => $this->comments,
            Post::CREATED_AT => $this->created_at->toDateTimeString(),
            Post::UPDATED_AT => $this->updated_at->toDateTimeString(),
        ];
    }
}
