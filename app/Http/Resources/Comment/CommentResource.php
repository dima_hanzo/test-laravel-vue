<?php

namespace App\Http\Resources\Comment;

use App\Models\Comment\Comment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            Comment::COLUMN_ID => $this->id,
            Comment::COLUMN_POST_ID => $this->post_id,
            Comment::COLUMN_PARENT_ID => $this->parent_id,
            Comment::COLUMN_TEXT => $this->text,
            Comment::FIELD_CHILD => $this->child,
            Comment::CREATED_AT => $this->created_at->toDateTimeString(),
            Comment::UPDATED_AT => $this->updated_at->toDateTimeString(),
        ];
    }
}
