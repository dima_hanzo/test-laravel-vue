<?php

namespace App\Repositories\Post;

use App\Models\Post\Post;
use App\Repositories\AbstractRepository;

class PostRepository extends AbstractRepository
{
    /**
     * @var string $class
     */
    protected $class = Post::class;
}
