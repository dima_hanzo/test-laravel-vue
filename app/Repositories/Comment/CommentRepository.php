<?php

namespace App\Repositories\Comment;

use App\Models\Comment\Comment;
use App\Repositories\AbstractRepository;

class CommentRepository extends AbstractRepository
{
    /**
     * @var string $class
     */
    protected $class = Comment::class;
}
