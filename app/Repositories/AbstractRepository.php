<?php

namespace App\Repositories;

use App\Contracts\RepositoryContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class AbstractRepository implements RepositoryContract
{
    /**
     * @var string $class
     */
    protected $class;

    /**
     * @var Model $model
     */
    private $model;

    /**
     * AbstractRepository constructor.
     */
    public function __construct()
    {
        if (class_exists($this->class) && is_subclass_of($this->class, Model::class, true)) {
            $this->setModel(new $this->class());
        } else {
            throw new \Exception('Property $class must be instance of Model!');
        }
    }

    /**
     * @param int $id
     * @return Model|null
     */
    public function get(int $id): ?Model
    {
        return $this->model::find($id);
    }

    /**
     * @param array $data
     * @return Model|null
     */
    public function create(array $data): ?Model
    {
        return $this->model::create($data);
    }

    /**
     * @param array $data
     * @param Model $model
     * @return Model
     */
    public function update(array $data, Model $model): Model
    {
        $model->fill($data)->save();
        return $model;
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function delete(int $id): bool
    {
        return $this->model::destroy($id);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model::all();
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @param Model $model
     */
    private function setModel(Model $model): void
    {
        $this->model = $model;
    }
}
