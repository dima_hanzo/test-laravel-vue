## Install

- `composer install`
- `npm install`
- `cp .env.example .env` and add your DB credentials and app URL
-  set up your api host in .env, ex. `MIX_API_HOST=http://test-laravel-vue/api/v1/`
- `php artisan key:generate`
- `php artisan config:cache`
- `php artisan cache:clear`
- `php artisan migrate --seed`
- `npm run prod`
- `php artisan serve` and open browser

P.S. - Я думаю что это не лучшая реализация дерева комментариев, но на реализацию nested set у меня не хватило времени:)
