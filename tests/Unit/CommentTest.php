<?php

namespace Tests\Unit;

use App\Models\Comment\Comment;
use App\Models\Post\Post;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use WithFaker;

    public function testCommentsCreateAndListedCorrectly()
    {
        /**
         * @var Post $post
         */
        $post = factory(Post::class, 1)->create()->each(function ($post) {
            $post->comments()->saveMany(
                factory(Comment::class, 3)
                    ->create([Comment::COLUMN_POST_ID => $post->id])
                    ->each(function ($comment) use ($post) {
                        $comment->child()->saveMany(
                            factory(Comment::class, 3)->create([
                                Comment::COLUMN_POST_ID => $post->id,
                            ]));
                    }));
        })->first();

        $this->get(route('posts.show', $post->id))
            ->assertStatus(200)
            ->assertJson([
                'data' => $post->load(['comments', 'comments.child'])->toArray()
            ])
            ->assertJsonStructure([
                'data' => [
                    Post::COLUMN_ID,
                    Post::COLUMN_TITLE,
                    Post::COLUMN_TEXT,
                    Post::FIELD_COMMENTS => [
                        '*' => [
                            Comment::COLUMN_ID,
                            Comment::COLUMN_PARENT_ID,
                            Comment::COLUMN_POST_ID,
                            Comment::COLUMN_TEXT,
                            Comment::FIELD_CHILD => [
                                '*' => [
                                    Comment::COLUMN_ID,
                                    Comment::COLUMN_PARENT_ID,
                                    Comment::COLUMN_POST_ID,
                                    Comment::COLUMN_TEXT,
                                    Comment::FIELD_CHILD,
                                    Comment::CREATED_AT,
                                    Comment::UPDATED_AT,
                                ]
                            ],
                            Comment::CREATED_AT,
                            Comment::UPDATED_AT,
                        ]
                    ],
                    Post::CREATED_AT,
                    Post::UPDATED_AT
                ],
            ]);
    }

    public function testAddCommentToPostCorrectly()
    {
        /**
         * @var Post $post
         */
        $post = factory(Post::class)->create();
        $comment = factory(Comment::class)->make([Comment::COLUMN_POST_ID => $post->id])->load('child')->toArray();

        $this->post(route('comments.store'), $comment)
            ->assertStatus(201)
            ->assertJson([
                'data' => $comment
            ])
            ->assertJsonStructure([
                'data' => [
                    Comment::COLUMN_ID,
                    Comment::COLUMN_PARENT_ID,
                    Comment::COLUMN_POST_ID,
                    Comment::COLUMN_TEXT,
                    Comment::FIELD_CHILD,
                    Comment::CREATED_AT,
                    Comment::UPDATED_AT,
                ]
            ]);
    }

    public function testReplyCommentCorrectly()
    {
        /**
         * @var Post $post
         */
        $post = factory(Post::class)->create();
        $commentParent = $post->comments()->create(factory(Comment::class)->make()->toArray());
        $comment = factory(Comment::class)->make([
            Comment::COLUMN_POST_ID => $post->id,
            Comment::COLUMN_PARENT_ID => $commentParent->id,
        ])->load('child')->toArray();

        $this->post(route('comments.store'), $comment)
            ->assertStatus(201)
            ->assertJson([
                'data' => $comment
            ])
            ->assertJsonStructure([
                'data' => [
                    Comment::COLUMN_ID,
                    Comment::COLUMN_PARENT_ID,
                    Comment::COLUMN_POST_ID,
                    Comment::COLUMN_TEXT,
                    Comment::FIELD_CHILD,
                    Comment::CREATED_AT,
                    Comment::UPDATED_AT,
                ]
            ]);
    }

    public function testUpdateCommentCorrectly()
    {
        /**
         * @var Post $post
         */
        $post = factory(Post::class)->create();
        $comment = $post->comments()->create(factory(Comment::class)->make()->toArray());

        $data = $comment->toArray();
        $data[Comment::COLUMN_TEXT] = $this->faker->paragraph;

        $this->put(route('comments.update', $comment->id), $data)
            ->assertStatus(200)
            ->assertJson([
                'data' => $data
            ])
            ->assertJsonStructure([
                'data' => [
                    Comment::COLUMN_ID,
                    Comment::COLUMN_PARENT_ID,
                    Comment::COLUMN_POST_ID,
                    Comment::COLUMN_TEXT,
                    Comment::FIELD_CHILD,
                    Comment::CREATED_AT,
                    Comment::UPDATED_AT,
                ]
            ]);
    }

    public function testDeleteCommentCorrectly()
    {
        /**
         * @var Post $post
         */
        $post = factory(Post::class)->create();
        $comment = $post->comments()->create(factory(Comment::class)->make()->toArray());

        $this->delete(route('comments.destroy', $comment->id))
            ->assertStatus(204)
            ->assertNoContent();
    }
}
