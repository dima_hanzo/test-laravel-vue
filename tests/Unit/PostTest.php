<?php

namespace Tests\Unit;

use App\Models\Post\Post;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    use WithFaker;

    public function testPostsCreateAndListedCorrectly()
    {
        $posts = factory(Post::class, 2)->create();

        $this->get(route('posts.index'))
            ->assertStatus(200)
            ->assertJson([
                'data' => $posts->each(function ($post) {
                    return $post->load(['comments']);
                })->toArray()
            ])
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        Post::COLUMN_ID,
                        Post::COLUMN_TITLE,
                        Post::COLUMN_TEXT,
                        Post::FIELD_COMMENTS,
                        Post::CREATED_AT,
                        Post::UPDATED_AT
                    ],
                ]
            ]);
    }

    public function testShowSinglePostCorrectly()
    {
        $post = factory(Post::class)->create();

        $this->get(route('posts.show', $post->id))
            ->assertStatus(200)
            ->assertJson([
                'data' => $post->load(['comments'])->toArray()
            ])
            ->assertJsonStructure([
                'data' => [
                    Post::COLUMN_ID,
                    Post::COLUMN_TITLE,
                    Post::COLUMN_TEXT,
                    Post::FIELD_COMMENTS,
                    Post::CREATED_AT,
                    Post::UPDATED_AT
                ]
            ]);
    }

    public function testShowSinglePostNotFound()
    {
        $this->get(route('posts.show', $this->faker->randomNumber()))
            ->assertStatus(404);
    }
}
