import axios from 'axios'

export class PostApi {

    constructor() {
        this.API_ENDPOINT = process.env.MIX_API_HOST + 'posts'
    }

    async addRecord(post) {
        return axios.post(this.API_ENDPOINT, post)
    }

    async updateRecord(record, id) {
        return axios.put(`${this.API_ENDPOINT}/${id}`,record)
    }

    async fetchById(id) {
        return axios.get(`${this.API_ENDPOINT}/${id}`)
    }

    async fetchAll() {
        return axios.get(this.API_ENDPOINT)
    }
}
