import axios from 'axios'

export class CommentApi {

    constructor() {
        this.API_ENDPOINT = process.env.MIX_API_HOST + 'comments'
    }

    async create(comment) {
        return axios.post(this.API_ENDPOINT, comment)
    }

    async update(comment, id) {
        return axios.put(`${this.API_ENDPOINT}/${id}`,comment)
    }

    async fetchById(id) {
        return axios.get(`${this.API_ENDPOINT}/${id}`)
    }

    async delete(id) {
        return axios.delete(`${this.API_ENDPOINT}/${id}`)
    }

    async fetchAll() {
        return axios.get(this.API_ENDPOINT)
    }
}
