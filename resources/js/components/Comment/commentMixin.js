export const commentMixin = {
    data() {
        return {
            dialogVisible: false,
            initialForm: {
                parent_id: null,
                post_id: this.$route.params.id,
                text: ''
            }
        }
    },
    methods: {
        showDialog(show = true) {
            this.dialogVisible = show
        },

    }
}
