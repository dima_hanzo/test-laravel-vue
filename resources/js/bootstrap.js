import VueRouter from 'vue-router'
import ElementUI from 'element-ui'
import VueAxios from 'vue-axios'
import axios from 'axios'
import {routes} from './includes/routes'
import 'element-ui/lib/theme-chalk/index.css';
// import {changeMomentLocale, momentPlugin} from '../utils/momentPlugin'

export function setupPlugins(Vue) {
    Vue.use(VueRouter)
    Vue.use(ElementUI)
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
    /**
     * Next we will register the CSRF Token as a common header with Axios so that
     * all outgoing HTTP requests automatically have it attached. This is just
     * a simple convenience so we don't have to attach every token manually.
     */

    let token = document.head.querySelector('meta[name="csrf-token"]')

    if (token) {
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token')
    }

    Vue.use(VueAxios, axios)
    // Vue.use(momentPlugin)

    Vue.axios.defaults.baseURL = process.env.MIX_API_HOST
}

export function createInstanceOptions(Vue) {
    const router = new VueRouter({
        routes,
        mode: 'history'
    })

    Vue.router = router

    return {
        router
    }
}
