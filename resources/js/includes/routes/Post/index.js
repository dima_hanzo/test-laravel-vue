import Posts from "../../../components/Post/Posts";
import Post from "../../../components/Post/Post";

export const POST_LIST = 'post.list'
export const POST_SHOW = 'post.show'

export const routes = [
    {
        path: 'posts',
        name: POST_LIST,
        component: Posts,
    },
    {
        path: 'posts/:id',
        name: POST_SHOW,
        component: Post,
    }
]
