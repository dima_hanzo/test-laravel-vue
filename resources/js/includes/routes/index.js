import Home from "../../components/Home";
import NotFound from '../../components/NotFound';
import * as fromRouteNames from './routeNames';
import * as fromPost from "./Post/index";

export const routes = [
    {
        path: '/',
        component: Home,
        children: [
            ...fromPost.routes,
        ],
    },
    {
        path: '*',
        component: NotFound,
        name: fromRouteNames.NOT_FOUND
    }
];
