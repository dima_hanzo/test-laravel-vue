import Vue from 'vue'
import App from './components/App'
import * as bootstrap from './bootstrap'

bootstrap.setupPlugins(Vue)

window.Vue = new Vue({
    ...bootstrap.createInstanceOptions(Vue),
    render: h => h(App)
}).$mount('#app')

